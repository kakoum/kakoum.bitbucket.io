var app = new Vue({
  el: '#app',
/*  data: {
    mailShow: false,
    numberShow: false
  },*/
  methods: {
    copy: function (what) {
      var copyText = document.querySelector("#" + what)
      copyText.select()
    	$("."+what+"-is-copied").show()
    	$("."+what+"-is-copied").fadeOut(1500, function() {})
      document.execCommand("Copy")
    }
  }
})

